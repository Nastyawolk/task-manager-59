package ru.t1.volkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.system.ServerVersionRequest;
import ru.t1.volkova.tm.dto.response.system.ServerVersionResponse;
import ru.t1.volkova.tm.event.ConsoleEvent;

import java.sql.SQLException;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    private static final String DESCRIPTION = "Show program version.";

    @NotNull
    private static final String NAME = "version";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionResponse response = getSystemEndpoint().getVersion(new ServerVersionRequest());
        System.out.println(response.getVersion());
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
