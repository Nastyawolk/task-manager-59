package ru.t1.volkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.model.IListener;
import ru.t1.volkova.tm.event.ConsoleEvent;

import java.sql.SQLException;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    private static final String DESCRIPTION = "Show commands list.";

    @NotNull
    private static final String NAME = "help";

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[HELP]");
        for (@NotNull final IListener command : getAbstractListeners())
            System.out.println(command.getName());
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
