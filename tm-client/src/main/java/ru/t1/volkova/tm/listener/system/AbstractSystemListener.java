package ru.t1.volkova.tm.listener.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.volkova.tm.api.service.ILoggerService;
import ru.t1.volkova.tm.api.service.IPropertyService;
import ru.t1.volkova.tm.listener.AbstractListener;

@Getter
@Setter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    public ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    @Getter
    private AbstractListener[] abstractListeners;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

}
