package ru.t1.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Task;

import java.sql.SQLException;

public interface IProjectTaskService {

    Task bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws Exception;

    void removeProjectById(@NotNull String userId, @Nullable String projectId) throws SQLException;

    @NotNull
    Task unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws SQLException;

}
