package ru.t1.volkova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import ru.t1.volkova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.volkova.tm.api.service.IPropertyService;
import ru.t1.volkova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.volkova.tm.dto.request.system.ServerVersionRequest;
import ru.t1.volkova.tm.dto.response.system.ServerAboutResponse;
import ru.t1.volkova.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndPoint implements ISystemEndpoint {

    @Override
    @NotNull
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        response.setHostName(propertyService.getServerHost());
        return response;
    }

    @Override
    @NotNull
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
