package ru.t1.volkova.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedDTOModel>
        extends AbstractDTORepository<M> implements IAbstractUserOwnedDTORepository<M> {

    @Override
    @Transactional
    public void add(@NotNull final M entity) {
        super.add(entity);
    }

    @Override
    @Transactional
    public void update(@NotNull final M entity) {
        super.update(entity);
    }

}
