package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository>
        implements ITaskDTOService {

    @NotNull
    @Getter
    @Autowired
    public ITaskDTORepository repository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final TaskDTO task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        task.setStatus(status);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        task.setStatus(status);
        repository.update(task);
        return task;
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeOneById(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        repository.removeOneByIndex(userId, index);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @NotNull
    @Override
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final TaskDTO task = repository.findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    @NotNull
    public TaskDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO task = repository.findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDTO> tasks = repository.findAllByProjectId(userId, projectId);
        return tasks;
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDTO> tasks = repository.findAll(userId);
        return tasks;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId,
                                 @Nullable final Comparator comparator
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDTO> tasks = repository.findAll(userId, comparator);
        return tasks;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

}
