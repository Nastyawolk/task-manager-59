package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.volkova.tm.api.service.dto.ISessionDTOService;
import ru.t1.volkova.tm.exception.entity.SessionNotFoundException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;
import ru.t1.volkova.tm.dto.model.SessionDTO;

import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository>
        implements ISessionDTOService {

    @NotNull
    @Getter
    @Autowired
    public ISessionDTORepository repository;

    @Override
    @Transactional
    public void add(@NotNull final SessionDTO entity) {
        repository.add(entity);
    }

    @NotNull
    @Override
    public SessionDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final SessionDTO session = repository.findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @NotNull
    @Override
    public SessionDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final SessionDTO session = repository.findOneByIndex(userId, index);
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<SessionDTO> sessions = repository.findAll(userId);
        if (sessions == null) throw new SessionNotFoundException();
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<SessionDTO> sessions = repository.findAll(userId);
        if (sessions == null) throw new SessionNotFoundException();
        return sessions;
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId,
                                    @NotNull final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<SessionDTO> sessions = repository.findAll(userId, comparator);
        if (sessions == null) throw new SessionNotFoundException();
        return sessions;
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        findOneById(userId, id);
        repository.removeOneById(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        findOneByIndex(userId, index);
        repository.removeOneByIndex(userId, index);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

}
