package ru.t1.volkova.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.model.IAbstractRepository;
import ru.t1.volkova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<E extends AbstractModel> implements IAbstractRepository<E> {

    @NotNull
    @PersistenceContext
    public EntityManager entityManager;

    @Override
    @Transactional
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    @Transactional
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

}
