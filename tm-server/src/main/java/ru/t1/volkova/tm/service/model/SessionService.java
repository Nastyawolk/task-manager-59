package ru.t1.volkova.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.model.ISessionRepository;
import ru.t1.volkova.tm.api.service.model.ISessionService;
import ru.t1.volkova.tm.exception.entity.SessionNotFoundException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;
import ru.t1.volkova.tm.model.Session;

import java.util.Comparator;
import java.util.List;

@Getter
@Service
@NoArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    @NotNull
    @Getter
    @Autowired
    public ISessionRepository repository;

    @Override
    @Transactional
    public void add(@NotNull final Session entity) {
        super.add(entity);
    }

    @NotNull
    @Override
    public Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Session session = repository.findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @NotNull
    @Override
    public Session findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session session = repository.findOneByIndex(userId, index);
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Session> sessions = repository.findAll(userId);
        if (sessions == null) throw new SessionNotFoundException();
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Session> sessions = repository.findAll(userId);
        if (sessions == null) throw new SessionNotFoundException();
        return sessions;
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId,
                                 @NotNull final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Session> sessions = repository.findAll(userId, comparator);
        if (sessions == null) throw new SessionNotFoundException();
        return sessions;
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        findOneById(userId, id);
        repository.removeOneById(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        findOneByIndex(userId, index);
        repository.removeOneByIndex(userId, index);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

}
