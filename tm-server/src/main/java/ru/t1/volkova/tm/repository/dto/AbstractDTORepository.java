package ru.t1.volkova.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTORepository<E extends AbstractModelDTO> implements IAbstractDTORepository<E> {

    @NotNull
    @PersistenceContext
    public EntityManager entityManager;

    @Override
    @Transactional
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    @Transactional
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

}
