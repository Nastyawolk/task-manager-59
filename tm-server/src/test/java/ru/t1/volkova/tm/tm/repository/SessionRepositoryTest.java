package ru.t1.volkova.tm.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.dto.model.SessionDTO;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SessionRepositoryTest extends AbstractSchemeTest  {

    private final static int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private static List<SessionDTO> sessionList = new ArrayList<>();

    @NotNull
    private final static List<String> userIdList = new ArrayList<>();

    @NotNull
    private static ISessionDTORepository getSessionDTORepository() {
        return context.getBean(ISessionDTORepository.class);
    }

    @NotNull
    private static IUserDTORepository getUserDTORepository() {
        return context.getBean(IUserDTORepository.class);
    }

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull UserDTO user = new UserDTO();
            user.setFirstName("User " + i);
            user.setLastName("User" + i);
            user.setMiddleName("User" + i);
            user.setEmail("user" + i + "@mail.ru");
            user.setLogin("user" + i);
            user.setPasswordHash("user" + i);
            userRepository.add(user);
            userIdList.add(user.getId());
        }
        for (final String userid : userIdList) {
            createSessions(userid);
        }
    }

    private static void createSessions(@NotNull final String userId)  {
        sessionList = new ArrayList<>();
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        @NotNull SessionDTO session = new SessionDTO();
        session.setRole(Role.USUAL);
        session.setUserId(userId);
        sessionRepository.add(session);
        sessionList.add(session);
    }

    @Test
    public void testAddSession() {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        int expectedNumberOfEntries = sessionRepository.getSize(userId) + 1;
        @NotNull final SessionDTO newSession = new SessionDTO();
        newSession.setUserId(userId);
        sessionRepository.add(newSession);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(userId));
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(userIdList.size());
        @NotNull final String userId = userIdList.get(index);
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        @Nullable final List<SessionDTO> sessions = sessionRepository.findAll(userId);
        assertEquals(sessions.size(), sessionRepository.getSize(userId));
    }

    @Test
    public void testFindAllForUserNegative() {
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        @Nullable final List<SessionDTO> sessions = sessionRepository.findAll((String) null);
        Assert.assertEquals(0, sessions.size());
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        for (@NotNull final SessionDTO session : sessionList) {
            assertEquals(session, sessionRepository.findOneById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        @NotNull final String userId = userIdList.get(0);
        Assert.assertNull(sessionRepository.findOneById(null, sessionList.get(0).getId()));
        Assert.assertNull(sessionRepository.findOneById(userId, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final String userId = userIdList.get(0);
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(userId, 0);
        Assert.assertNotNull(session);
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(userIdList.size());
        @NotNull final String userId = userIdList.get(index);
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        Assert.assertNull(sessionRepository.findOneByIndex(userId, NUMBER_OF_ENTRIES + 10));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(userIdList.size());
        @NotNull final String userId = userIdList.get(index);
        @NotNull final String id = sessionList.get(index).getId();
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        sessionRepository.removeOneById(userId, id);
        assertNull(sessionRepository.findOneById(userId, sessionList.get(index).getId()));
        createSessions(userId);
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(userIdList.size());
        @NotNull final String userId = userIdList.get(index);
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        final int size = sessionRepository.getSize(userId);
        sessionRepository.clear(userId);
        Assert.assertEquals(0, sessionRepository.getSize(userId));
        for (int i = 0; i < size; i++) {
            createSessions(userId);
        }
    }

    @Test
    public void testRemoveAllForUserNegative() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(userIdList.size());
        @NotNull final String userId = userIdList.get(index);
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        sessionRepository.clear("NotExcitingId");
        Assert.assertNotEquals(0, sessionRepository.getSize(userId));
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @NotNull final String userId = userIdList.get(0);
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(userId, 0);
        sessionRepository.removeOneByIndex(userId, 0);
        Assert.assertNull(sessionRepository.findOneById(userId, session.getId()));
        createSessions(userId);
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        @NotNull final ISessionDTORepository sessionRepository = getSessionDTORepository();
        Assert.assertNotEquals(0, sessionRepository.getSize(userId));
    }

}
